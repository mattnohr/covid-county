function eraseChart2021(selector) {
    var container = d3.select(selector)
    container.select("svg").remove()
}

function drawChart2021(selector, url, endShadingWidth) {
    var margin = { top: 20, right: 20, bottom: 20, left: 40 },
        width = 1200 - margin.left - margin.right,
        height = 600 - margin.top - margin.bottom;

    var parseDate = d3.time.format("%Y-%m-%d").parse,
        dateFormatter = d3.time.format("%m/%d");

    var x = d3.time.scale()
        .range([0, width]);

    var y = d3.scale.linear()
        .range([height, 0]);

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom")
        .tickFormat(dateFormatter);

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
        .tickFormat(d3.format("s"))

    var line = d3.svg.line()
        .x(function (d) { return x(d.date); })
        .y(function (d) { return y(d.cases); });

    var svg = d3.select(selector).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //make the chart resize on window resize / handle different size screens
    //use "parentNode" here to make sure I'm looking at the "svg" and not the "g"
    d3.select(svg.node().parentNode).call(responsivefy);

    //handle the data
    d3.csv(url, function (error, data) {
        if (error) throw error;

        data.forEach(function (d) {
            d.date = parseDate(d.date);
            d.cases = +d.cases;
        });

        data.sort(function (a, b) {
            return a.date - b.date;
        });

        x.domain([
            d3.time.day.offset(data[0].date, -5), //start 5 days early
            d3.time.day.offset(data[data.length - 1].date, 2), //end 2 days late just for padding
        ]);

        // y axis
        //extent() returns the min and max for an array
        casesMinMax = d3.extent(data, function (d) {
            return d.cases;
        });
        //special handling - never let the min be below 0
        if(casesMinMax[0] < 0) {
            casesMinMax[0] = 0
        }
        y.domain(casesMinMax).nice();

        //draw the axis labels
        svg.append("g")
            .attr("class", "axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis);

        //add shading based on learning model recommendations
        addGuideLines2021(svg, width, y);
        addShadedGuides2021(svg, width, height, y, 0);

        // add shading for past couple of days
        //addShadingToEnd(svg, height, width, endShadingWidth)

        // Add axis text - Cases per 100000
        addCasesLabel2021(svg, yAxis)

        // Draw the actual data line
        svg.append("path")
            .datum(data)
            .attr("class", "line")
            .attr("d", line);

        //draw circles for data points
        /*
        svg.selectAll("myCircles")
            .data(data)
            .enter()
            .append("circle")
            .attr("fill", "steelblue")
            .attr("stroke", "none")
            .attr("cx", function (d) { return x(d.date) })
            .attr("cy", function (d) { return y(d.cases) })
            .attr("r", 2)
        */

        // Handle tooltips
        tooltips(svg, width, height, x, y, data, dateFormatter);

        //add the latest value
        drawLatestValue(data, svg, width)
    });
}

function addGuideLines2021(svg, width, y) {
    var redLowerLimit = 100;

    svg.append("line")
        .attr("x1", 0)
        .attr("y1", y(10))
        .attr("x2", width)
        .attr("y2", y(10))
        .attr("class", "lineYellow");
    svg.append("line")
        .attr("x1", 0)
        .attr("y1", y(50))
        .attr("x2", width)
        .attr("y2", y(50))
        .attr("class", "lineOrange");

    if (y(redLowerLimit) > 0) {
        svg.append("line")
            .attr("x1", 0)
            .attr("y1", y(redLowerLimit))
            .attr("x2", width)
            .attr("y2", y(redLowerLimit))
            .attr("class", "lineRed");
    }
}

function addShadedGuides2021(svg, width, height, y, endShadingWidth) {
    var redLowerLimit = 100;
    var orangeLowerLimit = 50;
    var yellowLowerLimit = 10;

    //what is on top depends on how high this goes
    if (y(redLowerLimit) >= 0) {
        svg.append("rect")
            .attr("class", "shadeRed")
            .attr("width", width - endShadingWidth)
            .attr("x", 0)
            .attr("y", 0)
            .attr("height", y(redLowerLimit));
        svg.append("rect")
            .attr("class", "shadeOrange")
            .attr("width", width - endShadingWidth)
            .attr("x", 0)
            .attr("y", y(redLowerLimit))
            .attr("height", y(orangeLowerLimit) - y(redLowerLimit));
    } else {
        svg.append("rect")
            .attr("class", "shadeOrange")
            .attr("width", width - endShadingWidth)
            .attr("x", 0)
            .attr("y", 0)
            .attr("height", y(orangeLowerLimit));
    }
    svg.append("rect")
        .attr("class", "shadeYellow")
        .attr("width", width - endShadingWidth)
        .attr("x", 0)
        .attr("y", y(orangeLowerLimit))
        .attr("height", y(yellowLowerLimit) - y(orangeLowerLimit));
    svg.append("rect")
        .attr("class", "shadeBlue")
        .attr("width", width - endShadingWidth)
        .attr("x", 0)
        .attr("y", y(yellowLowerLimit))
        .attr("height", height - y(yellowLowerLimit));
}

function addCasesLabel2021(svg, yAxis) {
    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 15)
        .attr("dy", ".71em")
        .attr("x", -20)
        .style("text-anchor", "end")
        .text("Cases Per 100,000");
}