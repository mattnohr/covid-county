function drawChart(selector, url, endShadingWidth) {
    var margin = { top: 20, right: 20, bottom: 20, left: 40 },
        width = 1200 - margin.left - margin.right,
        height = 600 - margin.top - margin.bottom;

    var parseDate = d3.time.format("%Y-%m-%d").parse,
        dateFormatter = d3.time.format("%m/%d");

    var x = d3.time.scale()
        .range([0, width]);

    var y = d3.scale.linear()
        .range([height, 0]);

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom")
        .tickFormat(dateFormatter);

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
        .tickFormat(d3.format("s"))

    var line = d3.svg.line()
        .x(function (d) { return x(d.date); })
        .y(function (d) { return y(d.cases); });

    var svg = d3.select(selector).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //make the chart resize on window resize / handle different size screens
    //use "parentNode" here to make sure I'm looking at the "svg" and not the "g"
    d3.select(svg.node().parentNode).call(responsivefy);

    //handle the data
    d3.csv(url, function (error, data) {
        if (error) throw error;

        data.forEach(function (d) {
            d.date = parseDate(d.date);
            d.cases = +d.cases;
        });

        data.sort(function (a, b) {
            return a.date - b.date;
        });

        x.domain([
            d3.time.day.offset(data[0].date, -5), //start 5 days early
            d3.time.day.offset(data[data.length - 1].date, 2), //end 2 days late just for padding

        ]);
        y.domain(d3.extent(data, function (d) {
            return d.cases;
        })).nice();

        //draw the axis labels
        svg.append("g")
            .attr("class", "axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis);

        //add shading based on learning model recommendations
        addGuideLines(svg, width, y);
        addShadedGuides(svg, width, height, y, 0);

        // add shading for past couple of days
        //addShadingToEnd(svg, height, width, endShadingWidth)

        // Add axis text - Cases per 100000
        addCasesLabel(svg, yAxis)

        // Draw the actual data line
        svg.append("path")
            .datum(data)
            .attr("class", "line")
            .attr("d", line);

        //draw circles for data points
        /*
        svg.selectAll("myCircles")
            .data(data)
            .enter()
            .append("circle")
            .attr("fill", "steelblue")
            .attr("stroke", "none")
            .attr("cx", function (d) { return x(d.date) })
            .attr("cy", function (d) { return y(d.cases) })
            .attr("r", 2)
        */

        // Handle tooltips
        tooltips(svg, width, height, x, y, data, dateFormatter);

        //add the latest value
        drawLatestValue(data, svg, width)
    });
}

function tooltips(svg, width, height, x, y, data, dateFormatter) {
    var tooltipWidth = 150;
    var tooltipHeight = 50;
    var bisectDate = d3.bisector(function (d) { return d.date; }).left
    var formatValue = d3.format(".2f")

    var focus = svg.append("g")
        .attr("class", "focus")
        .style("display", "none");

    /*
    focus.append("circle")
        .attr("r", 5);
    */

    focus.append("rect")
        .attr("class", "tooltip")
        .attr("width", tooltipWidth)
        .attr("height", tooltipHeight)
        .attr("x", 10)
        .attr("y", -22)
        .attr("rx", 4)
        .attr("ry", 4);

    focus.append("text")
        .attr("class", "tooltip-date")
        .attr("x", 18)
        .attr("y", -2);

    focus.append("text")
        .attr("x", 18)
        .attr("y", 18)
        .text("Case Rate:");

    focus.append("text")
        .attr("class", "tooltip-cases")
        .attr("x", 90)
        .attr("y", 18);

    svg.append("rect")
        .attr("class", "overlay")
        .attr("width", width)
        .attr("height", height)
        .on("mouseover", function () { focus.style("display", null); })
        .on("mouseout", function () { focus.style("display", "none"); })
        .on("mousemove", mousemove);

    function mousemove() {
        var x0 = x.invert(d3.mouse(this)[0]),
            i = bisectDate(data, x0, 1),
            d0 = data[i - 1],
            d1 = data[i]

        if (d0 === undefined || d1 === undefined) {
            //console.log("Not doing anything");
        } else {
            var d = x0 - d0.date > d1.date - x0 ? d1 : d0;

            var xLoc = x(d.date);
            var yLoc = y(d.cases);

            //don't let the tooltip out of the chart
            if ((xLoc + tooltipWidth) > width) {
                xLoc = xLoc - tooltipWidth - 20
            }
            if ((yLoc - tooltipHeight) < 0) {
                yLoc = tooltipHeight
            }

            //console.log("Moving to " + xLoc + ", " + yLoc + " - width=" + width);
            focus.attr("transform", "translate(" + xLoc + "," + yLoc + ")");
            focus.select(".tooltip-date").text(dateFormatter(d.date));
            focus.select(".tooltip-cases").text(formatValue(d.cases));
        }
    }

}

function addGuideLines(svg, width, y) {
    var purpleLowerLimit = 49;
    var redLowerLimit = 29;

    svg.append("line")
        .attr("x1", 0)
        .attr("y1", y(9))
        .attr("x2", width)
        .attr("y2", y(9))
        .attr("class", "lineYellow");
    svg.append("line")
        .attr("x1", 0)
        .attr("y1", y(19))
        .attr("x2", width)
        .attr("y2", y(19))
        .attr("class", "lineOrange");

    if (y(redLowerLimit) > 0) {
        svg.append("line")
            .attr("x1", 0)
            .attr("y1", y(redLowerLimit))
            .attr("x2", width)
            .attr("y2", y(redLowerLimit))
            .attr("class", "lineRed");
    }
    if (y(purpleLowerLimit) > 0) {
        svg.append("line")
            .attr("x1", 0)
            .attr("y1", y(purpleLowerLimit))
            .attr("x2", width)
            .attr("y2", y(purpleLowerLimit))
            .attr("class", "linePurple");
    }
}

function addShadedGuides(svg, width, height, y, endShadingWidth) {
    var purpleLowerLimit = 49;
    var redLowerLimit = 29;
    var orangeLowerLimit = 19;
    var yellowLowerLimit = 9;

    //what is on top depends on how high this goes
    if (y(purpleLowerLimit) >= 0) {
        svg.append("rect")
            .attr("class", "shadePurple")
            .attr("width", width - endShadingWidth)
            .attr("x", 0)
            .attr("y", 0)
            .attr("height", y(purpleLowerLimit));
        svg.append("rect")
            .attr("class", "shadeRed")
            .attr("width", width - endShadingWidth)
            .attr("x", 0)
            .attr("y", y(purpleLowerLimit))
            .attr("height", y(redLowerLimit) - y(purpleLowerLimit));
    } else {
        svg.append("rect")
            .attr("class", "shadeRed")
            .attr("width", width - endShadingWidth)
            .attr("x", 0)
            .attr("y", 0)
            .attr("height", y(redLowerLimit));
    }
    svg.append("rect")
        .attr("class", "shadeOrange")
        .attr("width", width - endShadingWidth)
        .attr("x", 0)
        .attr("y", y(redLowerLimit))
        .attr("height", y(orangeLowerLimit) - y(redLowerLimit));
    svg.append("rect")
        .attr("class", "shadeYellow")
        .attr("width", width - endShadingWidth)
        .attr("x", 0)
        .attr("y", y(orangeLowerLimit))
        .attr("height", y(yellowLowerLimit) - y(orangeLowerLimit));
    svg.append("rect")
        .attr("class", "shadeGreen")
        .attr("width", width - endShadingWidth)
        .attr("x", 0)
        .attr("y", y(yellowLowerLimit))
        .attr("height", height - y(yellowLowerLimit));
}

function addShadingToEnd(svg, height, width, shadingWidth) {
    svg.append("rect")
        .attr("class", "lastDaysShading")
        .attr("width", shadingWidth)
        .attr("height", height)
        .attr("x", width - shadingWidth)
        .attr("y", 0)
}

function addCasesLabel(svg, yAxis) {
    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 15)
        .attr("dy", ".71em")
        .attr("x", -20)
        .style("text-anchor", "end")
        .text("Cases Per 10,000");
}

function drawLatestValue(data, svg, width) {
    var latestValue = data[data.length - 1].cases.toFixed(1)
    svg.append("g")
        .append("text")
        .attr("y", 10)
        .attr("dy", ".71em")
        .attr("x", width - 50)
        .style("text-anchor", "end")
        .text("Latest Value = " + latestValue);

}

function responsivefy(svg) {
    // get container + svg aspect ratio
    var container = d3.select(svg.node().parentNode),
        width = parseInt(svg.style("width")),
        height = parseInt(svg.style("height")),
        aspect = width / height;

    //console.log("container = " + container)

    // add viewBox and preserveAspectRatio properties,
    // and call resize so that svg resizes on inital page load
    svg.attr("viewBox", "0 0 " + width + " " + height)
        .attr("perserveAspectRatio", "xMinYMid")
        .call(resize);

    // to register multiple listeners for same event type,
    // you need to add namespace, i.e., 'click.foo'
    // necessary if you call invoke this function for multiple svgs
    // api docs: https://github.com/mbostock/d3/wiki/Selections#on
    d3.select(window).on("resize." + container.attr("id"), resize);

    // get width of container and resize svg to fit it
    function resize() {
        var targetWidth = parseInt(container.style("width"));

        if (!isNaN(targetWidth)) {
            //console.log("target width = " + targetWidth )
            if (targetWidth > 1000) {
                targetWidth = 1000
            }
            svg.attr("width", targetWidth);
            svg.attr("height", Math.round(targetWidth / aspect));
        }

    }
}
