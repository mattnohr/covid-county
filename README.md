# Ramsey County Covid Rates

[![pipeline status](https://gitlab.com/mattnohr/covid-county/badges/main/pipeline.svg)](https://gitlab.com/mattnohr/covid-county/-/commits/main)

Calculate the infection rate and display it on a chart

## GitLab Links

- Deployment on GitLab pages: https://mattnohr.gitlab.io/covid-county/
- Pipelines: https://gitlab.com/mattnohr/covid-county/-/pipelines
- Pipeline Schedules: https://gitlab.com/mattnohr/covid-county/-/pipeline_schedules

## Data Links

- Original news story: https://www.mprnews.org/story/2020/07/30/minnesota-officials-to-announce-fall-school-plans-thursday
- Ramsey County data: https://data.ramseycounty.us/dataset/COVID-19-New-Cases-by-Day/2u6p-f4uc
  - https://data.ramseycounty.us/api/views/sfe4-tpcz/rows.csv?accessType=DOWNLOAD
  - With Last updated date: https://data.ramseycounty.us/dataset/Cases-by-Specimen-Collection-Date/sfe4-tpcz
- New York Times data: https://developer.nytimes.com/covid
  - https://github.com/nytimes/covid-19-data
  - https://github.com/nytimes/covid-19-data/blob/master/us-counties.csv
- [Unused] John Hopkins: https://github.com/CSSEGISandData/COVID-19/

## Other Links

- Custom GitLab Badges: https://shields.io/ and https://medium.com/@iffi33/adding-custom-badges-to-gitlab-a9af8e3f3569

## New Year

NYT now stores the CSV files by year, presumably since they were getting so large. I have them downloaded in `data/mn-counties-YEAR.csv` which makes the processing much faster since the script does not need to download 49 states it does not use.

When there is a new year's worth of data, here are the steps to take:

1. Download the `us-counties-YEAR.csv` file from https://github.com/nytimes/covid-19-data
1. Add to the local `data` directory
1. Use the `util/NYTilter.kt` class (usage in the class) to create a new `data/mn-counties-YEAR.csv` file
1. Update `NYT.kt` to include the new year in local processing 