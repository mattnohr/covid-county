package covid.county

import covid.county.util.BadgeJson
import covid.county.util.CsvDownload
import covid.county.util.MinnesotaCounties
import java.io.File
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.Locale

class NYT {

    val EARLIST_YEAR = 2020
    val LATEST_YEAR = 2023
    val LOCAL_DATA_LATEST_YEAR = 2022

    fun getData() {
        // First delete any existing csv files
        deleteExistingCsvFiles()

        for(y in EARLIST_YEAR..LATEST_YEAR){
            getDataByYear(y)
        }
    }

    fun deleteExistingCsvFiles() {
        val buildDirectory = File("build")
        if(buildDirectory.exists()){
            for (nextFile in buildDirectory.listFiles()){
                if(!nextFile.isDirectory() && nextFile.name.indexOf(".csv") > 0){
                    nextFile.delete()
                }
            }
        }
    }

    fun getDataByYear(year: Int){
        var csvFile: File
        if(year <= LOCAL_DATA_LATEST_YEAR){
            val fileName = "data/mn-counties-${year}.csv"
            println("Using local file: $fileName")
            csvFile = File(fileName)
        } else {
            // println("Reading data for ${year}")
            val sourceLink = "https://github.com/nytimes/covid-19-data/raw/master/us-counties-${year}.csv"
            csvFile = CsvDownload().downloadCSVFile(sourceLink)
        }

        //all Minnesota counties
        for (nextCounty in MinnesotaCounties().getCounties()) {
            // println(nextCounty)
            getDataByCounty(csvFile, nextCounty.key, "Minnesota", nextCounty.value)
        }

        // cleanup
        if(year > LOCAL_DATA_LATEST_YEAR){
            println("Deleting ${csvFile.path}")
            csvFile.delete()
        }
    }

    private fun getDataByCounty(csvFile: File, county:String, state: String, population: Int) {
        val countyName = county.toLowerCase().replace(" ", "-").replace(".", "")
        val outputFile = File("build/nyt-${countyName}.csv")

        // read from a file if it exists
        var csvLines: MutableList<String>
        if(outputFile.exists()) {
            csvLines = outputFile.readLines().toMutableList()
        } else {
            //this is the first list - write the header
            csvLines = mutableListOf<String>("date,cases,daily")
        }

        // val csvFile = temporaryReadFromFile()
        val countyLines = filterCountyData(csvFile, county, state)
        // println("Found ${countyLines.size} days")

        // get the date and daily count as a map
        val dateCounts = mutableMapOf<String, Int>()
        var previousValue = 0
        countyLines.forEach { nextLine ->
                val parts = nextLine.split(",")
                val dateString = parts.get(0)
                val total = parts.get(4).toInt()
                val dailyCount = total - previousValue
                previousValue = total
                dateCounts.put(dateString, dailyCount)
        }

        var index = 0
        val caseCount = mutableListOf<Int>()
        
        dateCounts.forEach { key, value ->
            caseCount.add(value)

            //also calculate for the last 7 days
            if (index > 6) {
                var lastWeek = 0
                for (i in (index - 6)..index) {
                    lastWeek += caseCount[i]
                }
                val casesPer100k = lastWeek.toFloat() / (population.toFloat() / 100000)
                csvLines.add("$key,$casesPer100k,$value")
            }
            index++
        }

        //Write out the CSV file
        
        
        println("Writing results to ${outputFile.path}")
        outputFile.writeText(csvLines.joinToString("\n"))
    }

    private fun temporaryReadFromFile(): File {
        val fileName = "/Users/nohr/Downloads/us-counties.csv"
        return File(fileName)
    }

    private fun filterCountyData(csvFile: File, county: String, state: String): List<String> {
        // println("Filtering to just $county data...")
        val outputLines = mutableListOf<String>()
        val countyLookup = "$county,$state"
        csvFile.forEachLine() { nextLine ->
            if (nextLine.indexOf(countyLookup) > 0) {
                outputLines.add(nextLine)
            }
         }

         // println("Found ${outputLines.size} lines")
         return outputLines
    }

    private fun stringToDate(input: String, pattern: String): LocalDate {
        val formatter = DateTimeFormatter.ofPattern(pattern, Locale.ENGLISH)
        return LocalDate.parse(input, formatter)
    }
}
