/*
 * This Kotlin source file was generated by the Gradle 'init' task.
 */
package covid.county

import java.io.File
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.Locale

import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response

import covid.county.util.NYTFilter

class App() {
    companion object {
        @JvmStatic
        fun main(args : Array<String>) {
            App().getData()
        }
    }
    
    fun getData() {
        println("")
        println("New York Times...")
        println("=================")
        NYT().getData()

        // NYTFilter().filterYear(2022)
    }
}
