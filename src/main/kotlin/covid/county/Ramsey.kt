package covid.county

import covid.county.util.BadgeJson
import covid.county.util.CsvDownload
import java.io.File
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.Locale

class Ramsey {
    fun downloadRamseyCountyData() {
        val population = 550321
        val sourceLink = "https://data.ramseycounty.us/api/views/sfe4-tpcz/rows.csv?accessType=DOWNLOAD"
        val csvFile = CsvDownload().downloadCSVFile(sourceLink)
        // val csvFile = temporaryReadFromFile()

        val fileLines = csvFile.readLines()
        println("Found ${fileLines.size} lines")

        val dateSums = mutableMapOf<String, Int>()
        fileLines.forEachIndexed { i, nextLine ->
            if (i == 0) {
                // println("Headings: $nextLine")
            } else {
                val parts = nextLine.split(",")
                val dateString = parts.get(1)
                val countString = parts.get(0)

                // println("Existing: ${dateSums[dateString]}")
                if (dateSums[dateString] == null) {
                    // println("New $dateString")
                    dateSums.put(dateString, countString.toInt())
                } else {
                    val currentCount = dateSums[dateString] ?: 0
                    val newCount = currentCount + countString.toInt()
                    // println("New count: $newCount")
                    dateSums.put(dateString, newCount)
                }
            }
        }

        val sortedMap: MutableMap<String, Int> = LinkedHashMap()
        dateSums.entries.sortedBy { stringToDate(it.key, "MMMM d yyyy") }.forEach { sortedMap[it.key] = it.value }

        var index = 0
        val caseCount = mutableListOf<Int>()
        val csvLines = mutableListOf<String>("date,cases,daily")
        sortedMap.forEach { key, value ->
            caseCount.add(value)
            if (index > 13) {
                val date = stringToDate(key, "MMMM d yyyy")
                var lastTwoWeeks = 0
                for (i in (index - 13)..index) {
                    lastTwoWeeks += caseCount[i]
                }
                val casesPer10k = lastTwoWeeks.toFloat() / (population.toFloat() / 10000)
                csvLines.add("$date,$casesPer10k,$value")
            }
            index++
        }

        //Write out the CSV file
        val outputFile = File("build/data.csv")
        println("Writing results to ${outputFile.path}")
        outputFile.writeText(csvLines.joinToString("\n"))

        //Save details for GitLab badge
        val lastRecordParts = csvLines.last().split(",")
        BadgeJson().outputJsonForBadge("build/badge-ramsey.json", lastRecordParts[0], lastRecordParts[1])

        // cleanup
        println("Deleting ${csvFile.path}")
        csvFile.delete()
    }

    private fun stringToDate(input: String, pattern: String): LocalDate {
        val formatter = DateTimeFormatter.ofPattern(pattern, Locale.ENGLISH)
        return LocalDate.parse(input, formatter)
    }

    private fun temporaryReadFromFile(): File {
        val fileName = "/Users/nohr/Downloads/ramsey.csv"
        return File(fileName)
    }
}
