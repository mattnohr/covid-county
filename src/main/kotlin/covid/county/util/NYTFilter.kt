package covid.county.util

import java.io.File

/**
 * Just used to filter the full NYT data set to just minnesota counties
 * 
 * Usage:
 * NYTFilter().filterYear(2020)
 */
class NYTFilter {
    fun filterYear(year: Int) {
        val csvFile = File("data/us-counties-${year}.csv")
        val outputLines = mutableListOf<String>()
        csvFile.forEachLine() { nextLine ->
            if (nextLine.indexOf("Minnesota") > 0) {
                outputLines.add(nextLine)
            }
         }

        val outputFile = File("data/mn-counties-${year}.csv")
        outputFile.writeText("date,county,state,fips,cases,deaths\n")
        outputFile.writeText(outputLines.joinToString("\n"))
    }
}
